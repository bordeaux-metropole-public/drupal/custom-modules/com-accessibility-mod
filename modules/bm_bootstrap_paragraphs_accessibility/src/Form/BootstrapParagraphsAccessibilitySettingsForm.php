<?php

namespace Drupal\bm_bootstrap_paragraphs_accessibility\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\bm_accessibility\Form\AccessibilitySettingsForm;

/**
 * Class BootstrapParagraphsAccessibilitySettingsForm.
 *
 * The settings form of the bm_bootstrap_paragraphs_accessibility module.
 *
 * @package Drupal\bm_bootstrap_paragraphs_accessibility\Form
 */
class BootstrapParagraphsAccessibilitySettingsForm extends AccessibilitySettingsForm {

  const SETTINGS = 'bm_bootstrap_paragraphs_accessibility.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bm_bootstrap_paragraphs_accessibility_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getSettings() {
    return self::SETTINGS;
  }

  /**
   * {@inheritdoc}
   */
  protected function addFormElements(array &$form, $config) {
    $form['bootstrap_paragraphs_a11y'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Apply a11y adjustements to Bootstrap Paragraphs module'),
      '#default_value' => $config->get('bootstrap_paragraphs_a11y'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function saveFormElements(FormStateInterface $form_state, $config) {
    $config
      ->set('bootstrap_paragraphs_a11y', $form_state->getValue('bootstrap_paragraphs_a11y'))
      ->save();
  }

}
