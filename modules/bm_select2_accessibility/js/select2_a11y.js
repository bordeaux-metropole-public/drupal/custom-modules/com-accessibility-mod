/**
 * @file
 * JavaScript behaviors for Select2 to improve accessibility.
 * This script modifies Select2's multiple selection elements by:
 * - Updating the `aria-label` and `title` attributes to include the text for the remove button.
 * - Removing the `aria-describedby` attribute to clean up accessibility clutter.
 * - Making the remove button for each selected item focusable.
 */

(($, Drupal, once) => {
  Drupal.behaviors.CustomSelect2MultipleForm = {
    attach(context) {
      // Function to process each Select2 multiple selection element
      const processSelect2ElementMultiple = (element) => {
        // Use 'once' to ensure this behavior is applied only once to each element
        once('select2-handling-multiple', element).forEach((val) => {
          // Add an event listener for the 'focus' event on the element
          element.addEventListener('focus', () => {
            // Find the Select2 rendered list of selected items
            const selectmutation = val.querySelector(
              '.select2-selection__rendered',
            );
            // If there are any selected items (children) present
            if (selectmutation.children.length > 0) {
              // Iterate through each selected item (child)
              Array.from(selectmutation.children).forEach((child) => {
                // Get the remove button (first child element)
                const button = child.children.item(0);
                // Get the current 'title' and 'aria-label' of the remove button
                const title = child.children.item(0).getAttribute('title');
                const arialabel = child.children
                  .item(0)
                  .getAttribute('aria-label');

                // Check if 'aria-label' or 'title' does not include the item's title attribute
                // If not, update the button's 'title' and 'aria-label' to include it
                if (
                  !arialabel.includes(`${child.getAttribute('title')}`) ||
                  !title.includes(`${child.getAttribute('title')}`)
                ) {
                  button.setAttribute(
                    'title',
                    `${title} ${child.getAttribute('title')}`,
                  );
                  button.setAttribute(
                    'aria-label',
                    `${arialabel} ${child.getAttribute('title')}`,
                  );
                }

                // Make the button focusable by setting 'tabindex' to 0
                button.setAttribute('tabindex', '0');
                // Remove the 'aria-describedby' attribute from the button to reduce accessibility clutter
                button.removeAttribute('aria-describedby');
              });
            }
          });
        });
      };

      // Create a MutationObserver to watch for changes in the DOM structure
      const observer = new MutationObserver((mutationsList) => {
        // Loop through each mutation that the observer detects
        mutationsList.forEach((mutation) => {
          // Only act if the mutation is of type 'childList' (i.e., DOM nodes were added/removed)
          if (mutation.type === 'childList') {
            mutation.addedNodes.forEach((node) => {
              // If the added node is an element node
              if (node.nodeType === Node.ELEMENT_NODE) {
                // If the node is a Select2 multiple selection element, process it
                if (node.matches('.select2-selection--multiple')) {
                  processSelect2ElementMultiple(node);
                }

                // If the node has nested Select2 elements, process each nested element
                const nestedSelect2Elements = node.querySelectorAll(
                  '.select2-selection--multiple',
                );
                nestedSelect2Elements.forEach(processSelect2ElementMultiple);
              }
            });
          }
        });
      });

      // Start observing the context for any changes in the DOM
      observer.observe(context, { childList: true, subtree: true });

      // Find any existing Select2 multiple selection elements and process them
      const existingSelect2Elements = context.querySelectorAll(
        '.select2-selection--multiple',
      );
      existingSelect2Elements.forEach(processSelect2ElementMultiple);

      const searchFields = context.querySelectorAll('.select2-search__field');
      if (searchFields) {
        searchFields.forEach(function (searchField) {
          const label = searchField.closest('.form-item').querySelector('label').innerText.trim();
          searchField.setAttribute('aria-label', Drupal.t('Search: @caption', {
            '@caption': label,
          }));
          searchField.setAttribute('title', Drupal.t('Search: @caption', {
            '@caption': label,
          }));
        });
      }
    },
  };
})(jQuery, Drupal, once);
