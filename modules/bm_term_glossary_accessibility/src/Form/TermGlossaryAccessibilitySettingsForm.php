<?php

namespace Drupal\bm_term_glossary_accessibility\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\bm_accessibility\Form\AccessibilitySettingsForm;

/**
 * Class TermGlossaryAccessibilitySettingsForm.
 *
 * The settings form of the bm_term_glossary_accessibility module.
 *
 * @package Drupal\bm_term_glossary_accessibility\Form
 */
class TermGlossaryAccessibilitySettingsForm extends AccessibilitySettingsForm {

  const SETTINGS = 'bm_term_glossary_accessibility.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bm_term_glossary_accessibility_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getSettings() {
    return self::SETTINGS;
  }

  /**
   * {@inheritdoc}
   */
  protected function addFormElements(array &$form, $config) {
    $form['jquery_ui_dialog_a11y'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable jQuery UI dialog accessibility fixes (cache flush required).'),
      '#default_value' => $config->get('jquery_ui_dialog_a11y'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function saveFormElements(FormStateInterface $form_state, $config) {
    $config
      ->set('jquery_ui_dialog_a11y', $form_state->getValue('jquery_ui_dialog_a11y'))
      ->save();
  }

}
