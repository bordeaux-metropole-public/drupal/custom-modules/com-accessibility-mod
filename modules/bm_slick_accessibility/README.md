# Slick accessibility

Adds acessibility feature to slick carousel module

## Table of contents

- Requirements
- Installation
- Configuration
- Other necessary configurations

## Requirements
- [slick](https://www.drupal.org/project/slick)
- [bm_accessibility](https://gitlab.com/bordeaux-metropole-public/drupal/custom-modules/com-accessibility-mod)
- Media (core module)

## Installation
Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration
- Activate behaviour in /config/content/bm_accessibility/slick

## Other necessary configurations
- Add mandatory alt to image media
- Add a mandatory custom 'field_credit' and 'field_caption' fields to image media
- Add a slick configuration set
- Select 'field_credit' and 'field_caption' and 'field_caption' as 'Caption fields' in field display
- Translate Next en Previous arrows label in Slick's option sets to add
document type (Next 'image' or Next 'vidéo')
- Translate 'Zoom on image @image_name' in back office
