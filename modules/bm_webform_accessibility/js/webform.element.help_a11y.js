/**
 * @file
 * JavaScript behaviors for element help text (tooltip).
 */

(($, Drupal, once) => {
  // @see https://atomiks.github.io/tippyjs/v5/all-props/
  // @see https://atomiks.github.io/tippyjs/v6/all-props/
  Drupal.webform = Drupal.webform || {};
  Drupal.webform.elementHelpIcon = Drupal.webform.elementHelpIcon || {};
  Drupal.webform.elementHelpIcon.options =
    Drupal.webform.elementHelpIcon.options || {};

  // Hide on escape Tippy plugin.
  // @see https://atomiks.github.io/tippyjs/v6/plugins/#hideonesc
  const hideOnEsc = {
    name: 'hideOnEsc',
    defaultValue: true,
    fn: function fn(instance) {
      function onKeyDown(event) {
        if (event.keyCode === 27) {
          instance.hide();
        }
      }
      return {
        onShow: function onShow() {
          document.addEventListener('keydown', onKeyDown);
        },
        onHide: function onHide() {
          document.removeEventListener('keydown', onKeyDown);
        },
      };
    },
  };

  // Tippy plugin that closes the popper when it loses focus.
  // @see https://atomiks.github.io/tippyjs/v6/plugins/#hideonpopperblur
  const hideOnPopperBlur = {
    name: 'hideOnPopperBlur',
    defaultValue: true,
    fn(instance) {
      return {
        onCreate() {
          instance.popper.addEventListener('focusout', (event) => {
            if (
              instance.props.hideOnPopperBlur &&
              event.relatedTarget &&
              !instance.popper.contains(event.relatedTarget)
            ) {
              instance.hide();
            }
          });
        },
      };
    },
  };

  /**
   * Element help icon.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.webformElementHelpIcon = {
    attach(context) {
      if (!window.tippy) {
        return;
      }

      $(once('webform-element-help', '.js-webform-element-help', context)).each(
        function processEltHelp() {
          const $link = $(this);

          $link.on('click', (event) => {
            // Prevent click from toggling <label>s wrapped around help.
            event.preventDefault();
          });

          const options = $.extend(
            {
              content: $link.attr('data-webform-help'),
              delay: 100,
              allowHTML: true,
              interactive: true,
              // Accessibility fix: added aria-describedby.
              aria: {
                content: 'describedby',
              },
              // Accessibility fix: added appendTo.
              appendTo: 'parent',
              plugins: [hideOnEsc, hideOnPopperBlur],
            },
            Drupal.webform.elementHelpIcon.options,
          );

          window.tippy(this, options);
        },
      );
    },
  };

  Drupal.behaviors.webformElementHelpIconA11y = {
    processHelpButtonMutation(mutation) {
      if (mutation.type === 'childList') {
        const newNodes = mutation.addedNodes;
        newNodes.forEach((node) => {
          // Fix tooltip HTML only once.
          if (node.bm_a11y_updated) {
            return;
          }
          // Add the missing tooltip role to the tooltip DIV.
          node.setAttribute('role', 'tooltip');
          const helpTitle = node.querySelector('.webform-element-help--title');
          // If a help title exists but doesn't contain a P tag.
          if (helpTitle && !helpTitle.querySelector('p')) {
            helpTitle.innerHTML = `<p>${helpTitle.innerHTML}</p>`;
          }
          const helpContent = node.querySelector(
            '.webform-element-help--content',
          );
          // If a help content exists but doesn't contain a P tag.
          if (helpContent && !helpContent.querySelector('p,ol,ul')) {
            helpContent.innerHTML = `<p>${helpContent.innerHTML}</p>`;
          }
          node.bm_a11y_updated = true;
        });
      } else if (mutation.type === 'attributes') {
        // Remove unneeded aria-expanded attribute.
        mutation.target.removeAttribute(mutation.attributeName);
      }
    },
    attach(context) {
      const observer = new MutationObserver((mutations) => {
        mutations.forEach((mutation) => {
          this.processHelpButtonMutation(mutation);
        });
      });
      const helpButtons = once(
        'webform-element-help-a11y',
        '.js-webform-element-help',
        context,
      );
      $(helpButtons).each(function processEltHelp() {
        const helpButton = this;
        // Replace the tooltip role by a button role.
        helpButton.role = 'button';
        observer.observe(helpButton, {
          attributes: true,
          attributeFilter: ['aria-expanded'],
        });
        // Observe the addition of a tooltip to a webform element label.
        observer.observe(helpButton.parentNode, {
          childList: true,
        });
      });
    },
  };
})(jQuery, Drupal, once);
