<?php

namespace Drupal\bm_webform_accessibility\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\bm_accessibility\Form\AccessibilitySettingsForm;

/**
 * Class WebformAccessibilitySettingsForm.
 *
 * The settings form of the bm_webform_accessibility module.
 *
 * @package Drupal\bm_webform_accessibility\Form
 */
class WebformAccessibilitySettingsForm extends AccessibilitySettingsForm {

  const SETTINGS = 'bm_webform_accessibility.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bm_webform_accessibility_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getSettings() {
    return self::SETTINGS;
  }

  /**
   * {@inheritdoc}
   */
  protected function addFormElements(array &$form, $config) {
    $form['wizard_page_title'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Wizard page title fix (uniqueness)'),
      '#default_value' => $config->get('wizard_page_title'),
    ];

    $form['help_tooltip_aria'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add aria-describedby to help tooltip via template (cache flush required).'),
      '#default_value' => $config->get('help_tooltip_aria'),
    ];

    $form['help_tooltip_aria2'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add aria-describedby to help tooltip via library (cache flush required).'),
      '#default_value' => $config->get('help_tooltip_aria2'),
    ];

    $form['help_description_file'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Change description of file input in p'),
      '#default_value' => $config->get('help_description_file'),
    ];

    $form['aria_label_select2'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add a explicite aria-label when webform select2 is activated'),
      '#default_value' => $config->get('aria_label_select2'),
    ];

    $form['replace_css_asterisk'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Replace CSS asterisk by *'),
      '#default_value' => $config->get('replace_css_asterisk'),
      '#description' => $this->t('This setting replaces the CSS asterisk by *. Note: Additional CSS needs to be added in theme to style the asterisk appropriately.'),
    ];

    $form['delete_size_attribute_input'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Delete the size attribute on inputs tags'),
      '#default_value' => $config->get('delete_size_attribute_input'),
      '#description' => $this->t('This setting removes the size attribute attribute from input tags.
        For more details, see <a href=":url" target="_blank">accessibility guidelines</a>.', [
          ':url' => 'https://accessibilite.numerique.gouv.fr/methode/glossaire/#presentation-de-l-information',
        ]),
    ];

    $form['element_input_counter'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Count the last 20 characters on input limit'),
      '#default_value' => $config->get('element_input_counter'),
    ];

    $form['counter_aria_threshold'] = [
      '#type' => 'textfield',
      '#title' => $this->t('number to start the vocalization'),
      '#description' => $this->t('Specify the interval at which the vocal screen will start to vocalize'),
      '#default_value' => $config->get('counter_aria_threshold'),
      '#required' => TRUE,
      '#states' => [
        'visible' => [
          ':input[name="element_input_counter"]' => ['checked' => TRUE],
        ],
      ],
    ];

  }

  /**
   * {@inheritdoc}
   */
  protected function saveFormElements(FormStateInterface $form_state, $config) {
    $config
      ->set('wizard_page_title', $form_state->getValue('wizard_page_title'))
      ->set('help_tooltip_aria', $form_state->getValue('help_tooltip_aria'))
      ->set('help_tooltip_aria2', $form_state->getValue('help_tooltip_aria2'))
      ->set('aria_label_select2', $form_state->getValue('aria_label_select2'))
      ->set('replace_css_asterisk', $form_state->getValue('replace_css_asterisk'))
      ->set('help_description_file', $form_state->getValue('help_description_file'))
      ->set('delete_size_attribute_input', $form_state->getValue('delete_size_attribute_input'))
      ->set('element_input_counter', $form_state->getValue('element_input_counter'))
      ->set('counter_aria_threshold', $form_state->getValue('counter_aria_threshold'))
      ->save();
  }

}
