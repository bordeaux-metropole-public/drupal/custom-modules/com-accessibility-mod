<?php

namespace Drupal\bm_core_accessibility\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\bm_accessibility\Form\AccessibilitySettingsForm;

/**
 * Class CoreAccessibilitySettingsForm.
 *
 * The settings form of the bm_core_accessibility module.
 *
 * @package Drupal\bm_core_accessibility\Form
 */
class CoreAccessibilitySettingsForm extends AccessibilitySettingsForm {

  const SETTINGS = 'bm_core_accessibility.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'bm_core_accessibility_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getSettings() {
    return self::SETTINGS;
  }

  /**
   * {@inheritdoc}
   */
  protected function addFormElements(array &$form, $config) {
    $form['current_password_autocomplete'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add autocomplete current-password to current password field'),
      '#default_value' => $config->get('current_password_autocomplete'),
    ];

    $form['file_widget'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('File widgets focus handling accessibility improvements'),
      '#default_value' => $config->get('file_widget'),
    ];

    $form['inline_error_aria_describedby'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Link inline form error messages to their related form elements using aria-describedby attributes.'),
      '#default_value' => $config->get('inline_error_aria_describedby'),
    ];

    $form['add_aria_live_on_file_upload'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add aria-live="polite" on file upload'),
      '#default_value' => $config->get('add_aria_live_on_file_upload'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function saveFormElements(FormStateInterface $form_state, $config) {
    $config
      ->set('current_password_autocomplete', $form_state->getValue('current_password_autocomplete'))
      ->set('file_widget', $form_state->getValue('file_widget'))
      ->set('inline_error_aria_describedby', $form_state->getValue('inline_error_aria_describedby'))
      ->set('add_aria_live_on_file_upload', $form_state->getValue('add_aria_live_on_file_upload'))
      ->save();
  }

}
