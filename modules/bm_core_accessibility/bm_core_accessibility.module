<?php

/**
 * @file
 * Hook implementations for the bm_core_accessibility module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\bm_core_accessibility\Form\CoreAccessibilitySettingsForm;

/**
 * Implements hook_help().
 */
function bm_core_accessibility_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the bm_core_accessibility module.
    case 'help.page.bm_core_accessibility':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t("Module destiné à améliorer l'accessibilité du noyau Drupal.") . '</p>';
      return $output;

    default:
  }
}

/**
 * Implements hook_theme().
 */
function bm_core_accessibility_theme() {
  $config = \Drupal::config(CoreAccessibilitySettingsForm::SETTINGS);

  $info = [];

  if ($config->get('inline_error_aria_describedby')) {
    $info['form_element__a11y'] = [
      'template' => 'form-element--a11y',
      'base hook' => 'form_element',
    ];
  }
  if ($config->get('add_aria_live_on_file_upload')) {
    $info['file_managed_file__a11y'] = [
      'template' => 'file-managed-file--a11y',
      'base hook' => 'file_managed_file',
    ];
  }
  return $info;
}

/**
 * Implements hook_theme_suggestions_HOOK_alter().
 */
function bm_core_accessibility_theme_suggestions_alter(array &$suggestions, array $variables, $hook) {
  $config = \Drupal::config(CoreAccessibilitySettingsForm::SETTINGS);

  if ($hook === 'form_element') {
    if ($config->get('inline_error_aria_describedby')) {
      $suggestions[] = 'form_element__a11y';
    }
  }

  if ($hook === 'file_managed_file') {
    if ($config->get('add_aria_live_on_file_upload')) {
      $suggestions[] = 'file_managed_file__a11y';
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for user_form().
 */
function bm_core_accessibility_form_user_form_alter(&$form, FormStateInterface $form_state) {
  $config = \Drupal::config(CoreAccessibilitySettingsForm::SETTINGS);
  if ($config->get('current_password_autocomplete')) {
    if (isset($form['account']['current_pass'])) {
      $form['account']['current_pass']['#attributes']['autocomplete'] = 'current-password';
    }
  }
}

/**
 * Implements hook_preprocess_HOOK() for form element templates.
 */
function bm_core_accessibility_preprocess_form_element(&$variables) {
  $config = \Drupal::config(CoreAccessibilitySettingsForm::SETTINGS);
  if ($config->get('inline_error_aria_describedby')) {
    $element = $variables['element'];
    if (!empty($element['#errors']) && empty($element['#error_no_message'])) {
      $errors_id = $element['#id'] . '--error-message';
      $variables['error_message_id'] = $errors_id;
    }
  }
}

/**
 * Implements hook_preprocess_HOOK() for input templates.
 */
function bm_core_accessibility_preprocess_input(&$variables) {
  $config = \Drupal::config(CoreAccessibilitySettingsForm::SETTINGS);
  if ($config->get('inline_error_aria_describedby')) {
    $element = $variables['element'];
    if (!empty($element['#errors']) && empty($element['#error_no_message'])) {
      $errors_id = $element['#id'] . '--error-message';
      $current = $variables['attributes']['aria-describedby'] ?? NULL;
      $variables['attributes']['aria-describedby'] = $errors_id . ($current ? ' ' . $current : '');
    }
  }
}

/**
 * Implements hook_preprocess_HOOK().
 */
function bm_core_accessibility_preprocess_file_managed_file(&$variables) {
  if (!isset($variables['element']['#cardinality']) || $variables['element']['#cardinality'] === 1) {
    $multi_value = FALSE;
    if (isset($variables['element']['#multiple']) && $variables['element']['#multiple']) {
      $multi_value = TRUE;
    }

    _bm_core_accessibility_preprocess_file_widget($variables, FALSE, $multi_value);
  }

}

/**
 * Implements hook_preprocess_HOOK().
 */
function bm_core_accessibility_preprocess_file_widget_multiple(&$variables) {
  // Only triggers in entity_form with multi_value field.
  _bm_core_accessibility_preprocess_file_widget($variables, TRUE);
}

/**
 * Add some information to drupalSettings for focus handling improvements.
 *
 * @param array $variables
 *   The variables array.
 * @param bool $multiple_widget
 *   Flag depending on #cardinality of the field.
 * @param bool $multi_value
 *   Flag depending on #multiple of the field.
 */
function _bm_core_accessibility_preprocess_file_widget(array &$variables, bool $multiple_widget = FALSE, bool $multi_value = FALSE): void {
  $config = \Drupal::config(CoreAccessibilitySettingsForm::SETTINGS);
  if ($config->get('file_widget')) {
    $request = \Drupal::request();
    if ($request->isXmlHttpRequest()) {
      $pattern = '/(\w+)_(upload|remove)_button/';
      $trigger_elt_name = $request->request->get('_triggering_element_name');
      if (preg_match($pattern, $trigger_elt_name, $matches)) {
        $action = $matches[2];
        if ($multiple_widget && $action === 'upload') {
          $delta = $variables['element']['#file_upload_delta'];
          $element = $variables['element'][$delta - 1];
        }
        else {
          $element = $variables['element'];
        }
        $variables['#attached']['drupalSettings']['bm_core_accessibility']['fileWidgets'][] = [
          'action' => $action,
          'multiValue' => $multi_value,
          'name' => $element['#name'],
        ];

        if ($multiple_widget) {
          // TODO: manage multiple widget case.
        }
        else {
          $variables['#attached']['library'][] = 'bm_core_accessibility/file_widget';
        }
      }
    }
  }
}
